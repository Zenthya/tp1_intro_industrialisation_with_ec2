resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC2Lp92jVXtqmlUs/GK2I7feN4V+BEiQtznTT+hkxlhZqj6e29Pxl37NkxT8J5pomGGJ1H4F+va1FnKZCPFJcfGF3qh/wBk7WvSI7Hipzx+a3YHVDeLUSgtc9C+1UCoZ+N59dI09J/t+hb4twrysiU6M0C9domEy/bggcGSurHTPEc9z8jhvqG856gLtQD251aOmmS4R+dmTqgKB/AO3VG3SObaIfLLmDgSa4jdArxXLdyCi2AV9ZHmQmspHPBEEFpdkescYIR+9JpuPgz/b5PGUl4kue+SpHEVWqHWfYMtxKx64BDgWBOBR6TKeTcuF65CFheLOpQqmjDcxFUcPbwdpr4+TuS02mJmJkb7N2SXD+qnhzveRCvw1RPMEseIfVv22e7Zm59jfVrCG55IDN0J0bI+HKebL8Q3NQmii+mUSB8CmnFZkj+udJ/GjBmzL4RsNpVjEQNjCKhVvfoe+Fy0RVWyYa2EfezHWQ3Hv0jWwf4OGUyP4W+aAFElfsgkMUk= maxence@ubuntu"

}


resource "aws_default_vpc" "default" {
  tags = {
    Name = "Default VPC"
  }
}


resource "aws_default_security_group" "default" {
  vpc_id = aws_default_vpc.default.id

  ingress {
    protocol  =  6

    from_port = 22
    to_port   = 22


    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol  = 6

    from_port = 80
    to_port   = 5000


    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
        from_port   = 0
        to_port     = 0
        protocol    = -1
        cidr_blocks = ["0.0.0.0/0"]
    }



}
provider "aws" {
  region = "eu-west-1"
}



resource "aws_instance" "web" {
  ami           = var.ami_id
  instance_type = "t3.micro"
  security_groups = [aws_default_security_group.default.name]
  key_name = aws_key_pair.deployer.key_name

  tags = {
    Name = "HelloWorld"
  }
}
from flask import Flask
import boto3

app = Flask(__name__)


def insert_user_into_dynamo(user):

    dynamodb = boto3.resource("dynamodb")
    table = dynamodb.Table("User_info")
    print(table.creation_date_time)
    table.put_item(Item={"User_Id": user["id"]})


@app.route("/")
def hello():
    print("caca")
    return "Hello World!"


@app.route("/goodBye")
def goodbye():
    return "Good Bye World!"


if __name__ == "__main__":
    app.run(host="0.0.0.0")
